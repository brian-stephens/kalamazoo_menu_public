var express = require('express');
var router = express.Router();

var moment = require('moment')
var _ = require('lodash');

var conn = require('../config/db').connection();

const dotenv = require('dotenv');
dotenv.config();

var linkify = require('linkifyjs/html');

var async = require('async');

var mailjet = require('../config/config').mailjet();
var send_email = mailjet.post('send');

var session = require('express-session');

var recaptcha = require('../config/config').recaptcha();

var admin_url = require('../config/config').admin_url();

var session_secret = require('../config/config').session_secret();

router.use(session({
	secret: session_secret,
	resave: true,
	saveUninitialized: true,
	cookie: {
		maxAge: 1000 * 60 * 60 * 3
	}
 } )); // session secret

var flash = require('connect-flash');
router.use(flash()); // use connect-flash for flash messages stored in session

/* GET home page. */
router.get('/', function(req, res, next) {

	conn.query('SELECT * FROM cuisine WHERE active_ind = 1', function(err, cuisine) {
		conn.query('SELECT * FROM beverage WHERE active_ind = 1', function(err, beverage) {
			conn.query('SELECT * FROM diet WHERE active_ind = 1', function(err, diet) {
				if(err) console.log(err);

				var times = [];
				times.push({'name': 'Breakfast', 'ind' : 'breakfast_ind'});
				times.push({'name': 'Lunch', 'ind' : 'lunch_ind'});
				times.push({'name': 'Dinner', 'ind' : 'dinner_ind'});
				times.push({'name': 'Late', 'ind' : 'late_ind'});

				var info = req.flash('info');
				if(info.length >= 1) info = info[0];

				res.render('index', { title: 'Kalamazoo Menu', message: req.flash('message'), err_message: req.flash('err_message'), info: info, diet: diet, beverage: beverage, cuisine: cuisine, times: times});					
			})
		})
	})  	
});

router.post('/r_search', function(req, res, next) {

	var sql = 'SELECT * FROM restaurant WHERE approved_ind = 1';
	var body = req.body;
	var cuisine = [];
	var mealtime = [];
	var beverage = [];
	var diet = [];
	var foods = []

	length = Object.keys(body).length;

	conn.query('SELECT * FROM beverage', function(err, beverages) {
		conn.query('SELECT * FROM diet', function(err, diets) {

			async.forEachOf(body, function (item, key, cb) {

				// exclude these because we want them in the AND statement.  We want food types that are only available at certain times.
				if(key == 'breakfast_ind' || key == 'lunch_ind' || key == 'dinner_ind' || key == 'late_ind') {
					mealtime.push(key);
				} else if(beverages.some(e => e.ind === key)) {
					beverage.push(key);
				} else if(diets.some(e => e.ind === key)) {
					diet.push(key);
				} else {
					cuisine.push(key);
				}

				cb(null);

			}, function(err) {

				if(mealtime.length > 0) {
					sql += ' AND(';
					for(var i = 0; i < mealtime.length; i++) {
						sql += mealtime[i] += ' = 1';

						if(i !== mealtime.length - 1) {
							sql += ' OR ';
						} else {
							sql += ') '
						}
					}			
				}

				if(beverage.length > 0) {
					sql += ' AND(';
					for(var i = 0; i < beverage.length; i++) {
						sql += beverage[i] += ' = 1';

						if(i !== beverage.length - 1) {
							sql += ' OR ';
						} else {
							sql += ') '
						}
					}			
				}

				if(diet.length > 0) {
					sql += ' AND(';
					for(var i = 0; i < diet.length; i++) {
						sql += diet[i] += ' = 1';

						if(i !== diet.length - 1) {
							sql += ' OR ';
						} else {
							sql += ') '
						}
					}			
				}

				if(cuisine.length > 0) {
					sql += ' AND(';
					for(var i = 0; i < cuisine.length; i++) {
						sql += cuisine[i] += ' = 1';

						if(i !== cuisine.length - 1) {
							sql += ' OR ';
						} else {
							sql += ') '
						}
					}			
				}

				sql += ' ORDER BY RAND()';

				console.log('sql: ' + sql);
				conn.query(sql, function(err, results) {
					if(err) console.log('sql err: ', err);

					async.eachOf(results, function(rest, idx, cb) {
						delete results[idx].magic_link;
						delete results[idx].id;
						delete results[idx].approved_ind;
						delete results[idx].hidden_ind;
						delete results[idx].open_for_business_ind;
						delete results[idx].email;

						results[idx].special_promotions = linkify(rest.special_promotions);
						cb(null);
					}, function(err) {
						res.json(results);
					});		
				})
				
			});			
		})
	})



});

router.get('/r_search1', function(req, res, next) {
	var key = req.query.key;

	conn.query('SELECT id, name, ind FROM cuisine WHERE active_ind = 1 and name LIKE "%' + key + '%"' + 
				'UNION SELECT id, name, ind FROM beverage WHERE active_ind = 1 and name LIKE "%' + key + '%"' + 
				'UNION SELECT id, name, ind FROM diet WHERE active_ind = 1 and name LIKE "%' + key + '%"' +
				'UNION SELECT id, name, ind FROM third_party WHERE active_ind = 1 and name LIKE "%' + key + '%"' +
				'UNION SELECT id, name, null FROM restaurant WHERE approved_ind = 1 AND open_for_business_ind = 1 AND name LIKE "%' + key + '%"', function(err, results) {
		if(err) console.log(err);

		res.end(JSON.stringify(results));

	})
});

router.post('/r_search2', function(req, res, next) {
	var id = req.body.id;
	var name = req.body.name;

	conn.query('SELECT ind from cuisine where active_ind = 1 and name = ? UNION select ind from beverage where active_ind = 1 and name = ? UNION select ind from diet where active_ind = 1 and name = ? UNION SELECT ind from third_party where active_ind = 1 and name = ?', [name, name, name, name], function(err, results) {

		if(results.length > 0) {
			console.log('searching generic');
			conn.query('select * from restaurant where approved_ind = 1 AND open_for_business_ind = 1 AND ' + results[0].ind + ' = 1 ORDER BY RAND()', function(err, restaurants) {
				if(err) console.log('sql err: ', err);

				async.eachOf(restaurants, function(rest, idx, cb) {
					restaurants[idx].special_promotions = linkify(rest.special_promotions);
					delete restaurants[idx].magic_link;
					delete restaurants[idx].id;
					delete restaurants[idx].approved_ind;
					delete restaurants[idx].hidden_ind;
					delete restaurants[idx].open_for_business_ind;
					delete restaurants[idx].email;

					if(restaurants[idx].website_link != null && restaurants[idx].website_link != "") {
						if (!restaurants[idx].website_link.match(/^[a-zA-Z]+:\/\//)) {
						    restaurants[idx].website_link = 'https://' + restaurants[idx].website_link;
						}						
					}

					if(restaurants[idx].facebook_link != null && restaurants[idx].facebook_link != "") {
						if (!restaurants[idx].facebook_link.match(/^[a-zA-Z]+:\/\//)) {
						    restaurants[idx].facebook_link = 'https://' + restaurants[idx].facebook_link;
						}						
					}

					if(restaurants[idx].online_order_link != null && restaurants[idx].online_order_link != "") {
						if (!restaurants[idx].online_order_link.match(/^[a-zA-Z]+:\/\//)) {
						    restaurants[idx].online_order_link = 'https://' + restaurants[idx].online_order_link;
						}						
					}

					if(restaurants[idx].menu_link != null && restaurants[idx].menu_link != "") {
						if (!restaurants[idx].menu_link.match(/^[a-zA-Z]+:\/\//)) {
						    restaurants[idx].menu_link = 'https://' + restaurants[idx].menu_link;
						}						
					}


					cb(null);
				}, function(err) {
					res.json(restaurants);
				});
			})
		} else {
			console.log('searching restaurant');
			conn.query('select * from restaurant where approved_ind = 1 AND open_for_business_ind = 1 AND id = ? ORDER BY RAND()', id, function(err, restaurants) {
				if(err) console.log('sql err: ', err);

				async.eachOf(restaurants, function(rest, idx, cb) {
					restaurants[idx].special_promotions = linkify(rest.special_promotions);
					delete restaurants[idx].magic_link;
					delete restaurants[idx].id;
					delete restaurants[idx].approved_ind;
					delete restaurants[idx].hidden_ind;
					delete restaurants[idx].open_for_business_ind;
					delete restaurants[idx].email;

					if(restaurants[idx].website_link != null && restaurants[idx].website_link != "") {
						if (!restaurants[idx].website_link.match(/^[a-zA-Z]+:\/\//)) {
						    restaurants[idx].website_link = 'https://' + restaurants[idx].website_link;
						}						
					}

					if(restaurants[idx].facebook_link != null && restaurants[idx].facebook_link != "") {
						if (!restaurants[idx].facebook_link.match(/^[a-zA-Z]+:\/\//)) {
						    restaurants[idx].facebook_link = 'https://' + restaurants[idx].facebook_link;
						}						
					}

					if(restaurants[idx].online_order_link != null && restaurants[idx].online_order_link != "") {
						if (!restaurants[idx].online_order_link.match(/^[a-zA-Z]+:\/\//)) {
						    restaurants[idx].online_order_link = 'https://' + restaurants[idx].online_order_link;
						}						
					}

					if(restaurants[idx].menu_link != null && restaurants[idx].menu_link != "") {
						if (!restaurants[idx].menu_link.match(/^[a-zA-Z]+:\/\//)) {
						    restaurants[idx].menu_link = 'https://' + restaurants[idx].menu_link;
						}						
					}

					cb(null);
				}, function(err) {
					res.json(restaurants);
				});
					
			})
		}
	})
});

router.get('/new', function(req, res, next) {
	res.render('new_grub', {title: 'New Location'});
})

router.post('/new', function(req, res, next) {
		console.log('recaptcha: ', req.body)
	var randstring = '';

	randstring = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

	if(req.body['g-recaptcha-response'] === undefined || req.body['g-recaptcha-response'] === '' || req.body['g-recaptcha-response'] === null) {
		var info = {};
		info.new_email = req.body.new_email;

	    req.flash('err_message', 'Please fill out the captcha below.');
	    req.flash('info', info);

	    res.redirect('/#new_business');
	} else {
		recaptcha.validateRequest(req)
		.then(function(){
			var randstring = '';

			randstring = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

			conn.query('INSERT INTO restaurant(email,magic_link,approved_ind,hidden_ind) VALUES(?,?,0,0)', [req.body.new_email, randstring], function(err) {
				if(err) console.log(err);
				var emailData = {
				    'FromEmail': 'donotreply@kalamazoomenu.com',
				    'FromName': 'Kalamazoo Menu',
				    'Subject': 'Welcome to Kalamazoo Menu',
				    'Html-part': '<h2>Thanks for joining!</h2><p>We look forward to chowing down on some of your delicious food!' +
				    			 '  Please note that there may be a slight delay between adding your information and it showing up on the site as we check to ensure you are local to the city.' + 
				    			 '  When entering links such as your website or Facebook page, please use the full url including http or https as appropriate.  This will help ensure that customers' +
				    			 ' can click on your links!' +
				    			 '</p><br /><p>Please use the following link to access your personal business profile:  https://kalamazoomenu.com/admin/' + randstring + '</p>',
				    'Text-part': 'Kalamazoo Menu.  Thanks for joining!  We look forward to chowing down on some of your food!  Please use the following link to access your personal business profile:  https://kalamazoomenu.com/admin/' + randstring,
				    'Recipients': [{'Email': req.body.new_email}]
				}
				 
				send_email
					.request(emailData)
					.then(function() {
						req.flash('message', 'Check your email for a Magic Link!')
						res.redirect('/#new_business')				
					}

				);
				//req.flash('message', 'Check your email for a Magic Link!')
				// res.redirect('/#new_business')
			});
		})
		.catch(function(errorCodes){
			// invalid
			res.json({formSubmit:false, errors:recaptcha.translateErrors(errorCodes)});// translate error codes to human readable text
		});		
	}
});

router.get('/contact', function(req, res, next) {
	var info = req.flash('info');

	info.length >= 1 ? info = info[0] : info = "";
	res.render('contact', {message: req.flash('message'), info: info});
});

router.post('/contact', function(req, res, next) {

	if(req.body['g-recaptcha-response'] === undefined || req.body['g-recaptcha-response'] === '' || req.body['g-recaptcha-response'] === null) {
		var info = {};
		info.name = req.body.name;
		info.phone = req.body.phone;
		info.email = req.body.email;
		info.message = req.body.message;

	    req.flash('message', 'Please fill out the captcha below.');
	    req.flash('info', info);

	    res.redirect('/contact');
	} else {
		recaptcha.validateRequest(req)
		.then(function(){
			var emailData = {
			    'FromEmail': 'donotreply@kalamazoomenu.com',
			    'FromName': 'Kalamazoo Menu',
			    'Subject': 'Kalamazoo Menu Contact Request',
			    'Html-part': '<h2>Contact Us Request</h2><p>Name: ' + req.body.name + '</p><p>Phone: ' + req.body.phone + '</p><p>Email: ' + req.body.email + '</p><p>Message: ' + req.body.message,
			    'Recipients': [{'Email': 'kalamazoomenu@gmail.com'},{'Email': 'brian.a.stephens@gmail.com'}]
			}
			 
			send_email
				.request(emailData)
				.then(function() {
					req.flash('message', 'Thanks for reaching out, we will be in touch!');
					res.redirect('/contact#contact_form')				
				}

			);
			//req.flash('message', 'Check your email for a Magic Link!')
			// res.redirect('/#new_business')
		})
		.catch(function(errorCodes){
			// invalid
			res.json({formSubmit:false,errors:recaptcha.translateErrors(errorCodes)});// translate error codes to human readable text
		});		
	}

});


router.get('/admin/:id', function(req, res, next) {
	var magic_link = req.params.id;
	var deets = {};

	console.log('link: ' + magic_link);

	conn.query('SELECT * FROM restaurant WHERE magic_link = ?', magic_link, function(err, restaurant) {
		if(restaurant.length >= 1) {
			conn.query('SELECT * FROM third_party where active_ind = 1', function(err, third_party) {
				conn.query('SELECT * FROM cuisine where active_ind = 1', function(err, cuisine) {
					conn.query('SELECT * FROM beverage where active_ind = 1', function(err, beverage) {
						conn.query('SELECT * FROM diet where active_ind = 1', function(err, diet) {
							async.mapValues(restaurant[0], function(val, key, cb) {
								if(val != null && val != "") {
									if(key.search('_start') >= 0 || key.search('_end') >= 0) {
										deets[key] = moment(val).format('hh:mm a');
									} else {
										deets[key] = val;									
									}

								} else {
									deets[key] = "";
								}
								cb(null);
							}, function(err, mv_result) {
								res.render('admin', {result: deets, title: 'Admin', beverage: beverage, diet: diet, third_party: third_party, cuisine: cuisine})		
							})
						})
					})
				})
			})			
		} else {
			res.redirect('/');
		}

	});
});

router.post('/admin/update/:id', function(req, res, next) {
	var id = req.params.id;
	var body = req.body;
	var i = 0;
	length = Object.keys(body).length;

	var sql = 'UPDATE restaurant SET ';

	async.forEachOf(body, function (item, key, cb) {
		i++;

		if(key.search('_start') >= 0 || key.search('_end') >= 0) {
			if(item != '' && item != null) {
				sql += (key + ' = "' + moment(item, 'hh:mm a').format('YYYY-MM-DD HH:mm:ss') + '"');
				if(i !== length) {
					sql += ',';
				}				
			}


		} else {
			if(key != 'magic_link') {
				sql += (key + ' = "' + item + '"');
				if (i !== length) {
					sql += ',';
				}
			}			
		}



		cb(null);

	}, function (err) {
		sql += ' WHERE magic_link = "' + id + '"';
		conn.query(sql, function(err) {
			if(err) console.log(err)
			if(process.env.NODE_ENV == 'production') {
				conn.query('SELECT approved_ind FROM restaurant WHERE magic_link = "' + id + '"', function(err1, result) {
					if(err) console.log('update error: ', err);
					if(result[0].approved_ind != 1) {
						var emailData = {
						    'FromEmail': 'donotreply@kalamazoomenu.com',
						    'FromName': 'Kalamazoo Menu',
						    'Subject': 'New Business May Require Approval',
						    'Html-part': '<p>' + req.body.name + ' has updated their information but is not yet approved.</p>',
						    'Recipients': [{'Email': 'brian.a.stephens@gmail.com'}]
						}
						 
						send_email
							.request(emailData)
							.then(function() {
								res.redirect('/admin/' + req.body.magic_link);			
							}

						);				
					} else {
						res.redirect('/admin/' + req.body.magic_link);
					}
				});				
			} else {
				res.redirect('/admin' + req.body.magic_link);
			}
		})
	});

});

/***** Business Approvals *****/

router.get(admin_url, function(req, res, next) {
	conn.query('SELECT * FROM restaurant WHERE name IS NOT NULL AND hidden_ind != 1 ORDER BY approved_ind, name', function(err, restaurants) {
		res.render('siteadmin', {title: 'Admin' ,restaurants: restaurants})		
	});
});

router.post(admin_url, function(req, res, next) {
	console.log(req.body);
	conn.query('UPDATE restaurant SET approved_ind = ? WHERE id = ?',[req.body.val, req.body.id], function(err) {
		res.status(200).json({
			status: 'updated'
		});
	});
});


module.exports = router;
