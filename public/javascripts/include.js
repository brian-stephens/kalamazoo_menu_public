$('a[href^="#"], .scrolll, .nav-link').on('click', function(event) {
	var target = $(this.getAttribute('href'));
	if( target.length ) {
		event.preventDefault();
		$('html, body').stop().animate({
			scrollTop: target.offset().top
		}, 1000);
	}
});

$('#findfood').submit(function(event) {
	event.preventDefault();
	var form = $('#findfood')[0]
	var data = new FormData(form);
	//alert(JSON.stringify(Object.fromEntries(data)));

	$.ajax({
		type: 'POST',
		url: '/r_search',
		data: JSON.stringify(Object.fromEntries(data)),
		cache: false,
		processData: false,
		contentType: 'application/json',
		success: function(data) {
			$('#foodreturn').empty();

			if(data.length >= 1) {

				$(data).each(function() {
					var output = '';

					output +=
						'<div class="row mb-5" style="margin-bottom: 1rem !important;"' +
						'<div class="col-md-6 rest_row">' + 
						'<h3 style="font-weight: bold; padding-left: 5px;">' + $(this)[0].name + '</h3>' +
						'</div></div>' +
						'<div class="row mb-5">' +
						'<div class="col-md-6 rest_row">';

					output +=
						'<p><span style="font-weight: bold;"> Address: </span>' + $(this)[0].address + '</p>' +
						'<p><span style="font-weight: bold;"> Phone:  </span>' + $(this)[0].phone_number + '</p>';
					if(!($(this)[0].website_link == "" || $(this)[0].website_link == null)) {
						output += '<p><span style="font-weight: bold;"> Website:  </span><a href="' + $(this)[0].website_link + '">Click me!</a></p>'
					}

					if(!($(this)[0].online_order_link == "" || $(this)[0].online_order_link == null)){
						output += '<p><span style="font-weight: bold;"> Order online:   </span><a href="' + $(this)[0].online_order_link + '">Click me!</a></p>';
					}

					if(!($(this)[0].menu_link == "" || $(this)[0].menu_link == null)) {
						output += '<p><span style="font-weight: bold;"> Online menu:  </span><a href="' + $(this)[0].menu_link + '">Click me!</a></p>'
					}

					if(!($(this)[0].facebook_link == "" || $(this)[0].facebook_link == null)){
						output += '<p><span style="font-weight: bold;"> Facebook:   </span><a href="' + $(this)[0].facebook_link + '">Click me!</a></p>';
					}

					if($(this)[0].ubereats_ind == 1 || $(this)[0].grubhub_ind == 1 || $(this)[0].postmates_ind == 1 || $(this)[0].foodbooking_ind == 1) {

						var delivery = [];
						var delivery_out = '';

						if($(this)[0].ubereats_ind == 1) delivery.push('Uber Eats');
						if($(this)[0].grubhub_ind == 1) delivery.push('Grubhub');
						if($(this)[0].postmates_ind == 1) delivery.push('Postmates');
						if($(this)[0].foodbooking_ind == 1) delivery.push('Foodbooking');

						for(i = 0; i < delivery.length; i++) {
							delivery_out += delivery[i];
							if(i != delivery.length - 1) delivery_out += ", ";
						}

						output += '<p><span style="font-weight: bold;"> Delivery options:  </span>' + delivery_out + '</p>';

						output += '<p>';

					}

					Object.entries($(this)[0]).forEach(([key, value]) => {
						if(key == 'steak_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Steak</span>';
						if(key == 'american_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">American</span>';
						if(key == 'asian_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Asian</span>';
						if(key == 'baked_goods_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Bakery</span>';
						if(key == 'bbq_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">BBQ</span>';
						if(key == 'breakfast_brunch_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Breakfast</span>';
						if(key == 'ice_cream_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Ice Cream</span>';
						if(key == 'indian_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Indian</span>';
						if(key == 'italian_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Italian</span>';
						if(key == 'mexican_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Mexican</span>';
						if(key == 'pizza_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Pizza</span>';
						if(key == 'pub_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Pub Food</span>';
						if(key == 'sub_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Subs</span>';
						if(key == 'seafood_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Seafood</span>';
						if(key == 'soup_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Soup</span>';
						if(key == 'sushi_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Sushi</span>';
						if(key == 'misc_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Misc</span>';
					});

					output += '</p>';
					output +=
						'</div>' +
						'<div class="col-md-6">';

					output += '<p style="margin: 0 !important;"><span style="font-weight: bold;"> Monday hours:  </span>';
					if($(this)[0].mon_start == "" || $(this)[0].mon_start == null) {
						output += 'Closed';
					} else {
						output += moment.utc($(this)[0].mon_start).format('h:mm a') + ' - ' + moment.utc($(this)[0].mon_end).format('h:mm a') + '</p>';
					}

					output += '<p style="margin: 0 !important;"><span style="font-weight: bold;"> Tuesday hours:  </span>';
					if($(this)[0].tue_start == "" || $(this)[0].tue_start == null) {
						output += 'Closed';
					} else {
						output += moment.utc($(this)[0].tue_start).format('h:mm a') + ' - ' + moment.utc($(this)[0].tue_end).format('h:mm a') + '</p>';
					}

					output += '<p style="margin: 0 !important;"><span style="font-weight: bold;"> Wednesday hours:  </span>';
					if($(this)[0].wed_start == "" || $(this)[0].wed_start == null) {
						output += 'Closed';
					} else {
						output += moment.utc($(this)[0].wed_start).format('h:mm a') + ' - ' + moment.utc($(this)[0].wed_end).format('h:mm a') + '</p>';
					}

					output += '<p style="margin: 0 !important;"><span style="font-weight: bold;"> Thursday hours:  </span>';
					if($(this)[0].thu_start == "" || $(this)[0].thu_start == null) {
						output += 'Closed';
					} else {
						output += moment.utc($(this)[0].thu_start).format('h:mm a') + ' - ' + moment.utc($(this)[0].thu_end).format('h:mm a') + '</p>';
					}

					output += '<p style="margin: 0 !important;"><span style="font-weight: bold;"> Friday hours:  </span>';
					if($(this)[0].fri_start == "" || $(this)[0].fri_start == null) {
						output += 'Closed';
					} else {
						output += moment.utc($(this)[0].fri_start).format('h:mm a') + ' - ' + moment.utc($(this)[0].fri_end).format('h:mm a') + '</p>';
					}

					output += '<p style="margin: 0 !important;"><span style="font-weight: bold;"> Saturday hours:  </span>';
					if($(this)[0].sat_start == "" || $(this)[0].sat_start == null) {
						output += 'Closed';
					} else {
						output += moment.utc($(this)[0].sat_start).format('h:mm a') + ' - ' + moment.utc($(this)[0].sat_end).format('h:mm a') + '</p>';
					}

					output += '<p style="margin: 0 !important;"><span style="font-weight: bold;"> Sunday hours:  </span>';
					if($(this)[0].sun_start == "" || $(this)[0].sun_start == null) {
						output += 'Closed';
					} else {
						output += moment.utc($(this)[0].sun_start).format('h:mm a') + ' - ' + moment.utc($(this)[0].sun_end).format('h:mm a') + '</p>';
					}

					output +=	
						'<p style="margin-top: 30px !important;"><span style="font-weight: bold;">Current specials</span><br />' +
						$(this)[0].special_promotions + '</p>' +
						'</div></div>';
					$('#foodreturn').append(output);
					$('#find_food').addClass('bottom-slant-gray');
					$('#foodreturnsection').slideDown('fast')
					$('html, body').stop().animate({
						scrollTop: $('#foodreturn').offset().top
					}, 400);								
				});
			} else {
				var output = '';
				output +=
					'<div class="row mb-5" style="margin-bottom: 1rem !important;">' +
					'<div class="col-md-8 mx-auto rest_row">' + 
					'<p style="text-align: center; font-size: 22px;">No businesses match that search...yet!</p><p style="text-align: center; font-size: 22px;">Spread the word and have new local businesses join up!</p>' +
					'</div></div>';

				$('#foodreturn').append(output);
				$('#find_food').addClass('bottom-slant-gray');
				$('#foodreturnsection').slideDown('fast')
				$('html, body').stop().animate({
					scrollTop: $('#foodreturn').offset().top - 50
				}, 400);	
			}


		}
	});

});

var results = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  remote: {
	wildcard: '%QUERY',
	url: '/r_search1?key=%QUERY'
  }
});

results.initialize();

// Instantiate the Typeahead UI
$('.typeahead').typeahead(null, {
  name: 'results',
  display: function(item) {
  	return item.name
  },
  limit: 3,
  source: results.ttAdapter()
});

$('.typeahead').on('typeahead:select typeahead:autocompleted', function(event, item) {
	var data_out = {};

	if(typeof(item) == 'undefined') {
		data_out.id = $('#hid_type_in').val();
	} else {
		$('#hid_type_in').val(item.id);
		data_out.id = item.id;
	}

	data_out.name = $('#typeahead_input').val();

	if($('#typeahead_input').val() == "") {
		event.preventDefault();
	}

	$.ajax({
		type: 'POST',
		url: '/r_search2',
		contentType: 'application/json',
		data: JSON.stringify(data_out),
		cache: false,
		processData: false,
		success: function(data) {

			$('#foodreturn').empty();

			if(data.length >= 1) {
				$(data).each(function() {
					var output = '';

					output +=
						'<div class="row mb-5" style="margin-bottom: 1rem !important;">' +
						'<div class="col-md-6 rest_row">' + 
						'<h3 style="font-weight: bold; padding-left: 5px;">' + $(this)[0].name + '</h3>' +
						'</div></div>' +
						'<div class="row mb-5">' +
						'<div class="col-md-6 rest_row">';

					output +=
						'<p><span style="font-weight: bold;"> Address: </span>' + $(this)[0].address + '</p>' +
						'<p><span style="font-weight: bold;"> Phone:  </span>' + $(this)[0].phone_number + '</p>';
					if(!($(this)[0].website_link == "" || $(this)[0].website_link == null)) {
						output += '<p><span style="font-weight: bold;"> Website:  </span><a href="' + $(this)[0].website_link + '">Click me!</a></p>'
					}

					if(!($(this)[0].online_order_link == "" || $(this)[0].online_order_link == null)){
						output += '<p><span style="font-weight: bold;"> Order online:   </span><a href="' + $(this)[0].online_order_link + '">Click me!</a></p>';
					}

					if(!($(this)[0].menu_link == "" || $(this)[0].menu_link == null)) {
						output += '<p><span style="font-weight: bold;"> Online menu:  </span><a href="' + $(this)[0].menu_link + '">Click me!</a></p>'
					}

					if(!($(this)[0].facebook_link == "" || $(this)[0].facebook_link == null)){
						output += '<p><span style="font-weight: bold;"> Facebook:   </span><a href="' + $(this)[0].facebook_link + '">Click me!</a></p>';
					}

					if($(this)[0].ubereats_ind == 1 || $(this)[0].grubhub_ind == 1 || $(this)[0].postmates_ind == 1 || $(this)[0].foodbooking_ind == 1) {

						var delivery = [];
						var delivery_out = '';

						if($(this)[0].ubereats_ind == 1) delivery.push('Uber Eats');
						if($(this)[0].grubhub_ind == 1) delivery.push('Grubhub');
						if($(this)[0].postmates_ind == 1) delivery.push('Postmates');
						if($(this)[0].foodbooking_ind == 1) delivery.push('Foodbooking');

						for(i = 0; i < delivery.length; i++) {
							delivery_out += delivery[i];
							if(i != delivery.length - 1) delivery_out += ", ";
						}

						output += '<p><span style="font-weight: bold;"> Delivery options:  </span>' + delivery_out + '</p>';

						output += '<p>'

					}

					Object.entries($(this)[0]).forEach(([key, value]) => {
						if(key == 'steak_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Steak</span>';
						if(key == 'american_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">American</span>';
						if(key == 'asian_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Asian</span>';
						if(key == 'baked_goods_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Bakery</span>';
						if(key == 'bbq_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">BBQ</span>';
						if(key == 'breakfast_brunch_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Breakfast</span>';
						if(key == 'ice_cream_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Ice Cream</span>';
						if(key == 'indian_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Indian</span>';
						if(key == 'italian_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Italian</span>';
						if(key == 'mexican_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Mexican</span>';
						if(key == 'pizza_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Pizza</span>';
						if(key == 'pub_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Pub Food</span>';
						if(key == 'sub_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Subs</span>';
						if(key == 'seafood_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Seafood</span>';
						if(key == 'soup_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Soup</span>';
						if(key == 'sushi_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Sushi</span>';
						if(key == 'misc_ind' && value == 1) output += '<span style="border-radius: 10px !important;" class="btn btn-primary btn-foodlabel">Misc</span>';
					});

					output += '</p>';
					output +=
						'</div>' +
						'<div class="col-md-6" id="business_hours">';

					output += '<p style="margin: 0 !important;"><span style="font-weight: bold;"> Monday hours:  </span>';
					if($(this)[0].mon_start == "" || $(this)[0].mon_start == null || $(this)[0].mon_end == "" || $(this)[0].mon_end == null) {
						output += 'Closed';
					} else {
						output += moment.utc($(this)[0].mon_start).format('h:mm a') + ' - ' + moment.utc($(this)[0].mon_end).format('h:mm a') + '</p>';
					}

					output += '<p style="margin: 0 !important;"><span style="font-weight: bold;"> Tuesday hours:  </span>';
					if($(this)[0].tue_start == "" || $(this)[0].tue_start == null || $(this)[0].tue_end == "" || $(this)[0].tue_end == null) {
						output += 'Closed';
					} else {
						output += moment.utc($(this)[0].tue_start).format('h:mm a') + ' - ' + moment.utc($(this)[0].tue_end).format('h:mm a') + '</p>';
					}

					output += '<p style="margin: 0 !important;"><span style="font-weight: bold;"> Wednesday hours:  </span>';
					if($(this)[0].wed_start == "" || $(this)[0].wed_start == null || $(this)[0].wed_end == "" || $(this)[0].wed_end == null) {
						output += 'Closed';
					} else {
						output += moment.utc($(this)[0].wed_start).format('h:mm a') + ' - ' + moment.utc($(this)[0].wed_end).format('h:mm a') + '</p>';
					}

					output += '<p style="margin: 0 !important;"><span style="font-weight: bold;"> Thursday hours:  </span>';
					if($(this)[0].thu_start == "" || $(this)[0].thu_start == null || $(this)[0].thu_end == "" || $(this)[0].thu_end == null) {
						output += 'Closed';
					} else {
						output += moment.utc($(this)[0].thu_start).format('h:mm a') + ' - ' + moment.utc($(this)[0].thu_end).format('h:mm a') + '</p>';
					}

					output += '<p style="margin: 0 !important;"><span style="font-weight: bold;"> Friday hours:  </span>';
					if($(this)[0].fri_start == "" || $(this)[0].fri_start == null || $(this)[0].fri_end == "" || $(this)[0].fri_end == null) {
						output += 'Closed';
					} else {
						output += moment.utc($(this)[0].fri_start).format('h:mm a') + ' - ' + moment.utc($(this)[0].fri_end).format('h:mm a') + '</p>';
					}

					output += '<p style="margin: 0 !important;"><span style="font-weight: bold;"> Saturday hours:  </span>';
					if($(this)[0].sat_start == "" || $(this)[0].sat_start == null || $(this)[0].sat_end == "" || $(this)[0].sat_end == null) {
						output += 'Closed';
					} else {
						output += moment.utc($(this)[0].sat_start).format('h:mm a') + ' - ' + moment.utc($(this)[0].sat_end).format('h:mm a') + '</p>';
					}

					output += '<p style="margin: 0 !important;"><span style="font-weight: bold;"> Sunday hours:  </span>';
					if($(this)[0].sun_start == "" || $(this)[0].sun_start == null || $(this)[0].sun_end == "" || $(this)[0].sun_end == null) {
						output += 'Closed';
					} else {
						output += moment.utc($(this)[0].sun_start).format('h:mm a') + ' - ' + moment.utc($(this)[0].sun_end).format('h:mm a') + '</p>';
					}
					

					output +=	
						'<p style="margin-top: 30px !important;"><span style="font-weight: bold;">Current specials</span><br />' +
						$(this)[0].special_promotions + '</p>' +
						'</div></div>';

					$('#foodreturn').append(output);
					$('#find_food').addClass('bottom-slant-gray');
					$('#foodreturnsection').slideDown('fast')
					$('html, body').stop().animate({
						scrollTop: $('#foodreturn').offset().top
					}, 400);								
				});
			} else {

				var output = '';
				output +=
					'<div class="row mb-5" style="margin-bottom: 1rem !important;">' +
					'<div class="col-md-8 mx-auto rest_row">' + 
					'<p style="text-align: center; font-size: 22px;">No businesses match that search...yet!</p><p style="text-align: center; font-size: 22px;">Spread the word and have new local businesses join up!</p>' +
					'</div></div>';

				$('#foodreturn').append(output);
				$('#find_food').addClass('bottom-slant-gray');
				$('#foodreturnsection').slideDown('fast')
				$('html, body').stop().animate({
					scrollTop: $('#foodreturn').offset().top - 50
				}, 400);	
			}



		}
	});
});

$('#r_search_btn').on('click', function() {$('.typeahead').trigger('typeahead:select');});