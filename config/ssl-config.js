var fs = require('fs');

module.exports = {
	cert : fs.readFileSync('/etc/letsencrypt/live/lstfo.com/fullchain.pem').toString(),
	key : fs.readFileSync('/etc/letsencrypt/live/lstfo.com/privkey.pem').toString()
};